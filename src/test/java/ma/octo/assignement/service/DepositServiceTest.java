package ma.octo.assignement.service;

import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class DepositServiceTest {

    @Autowired
    private DepositService depositService;


    @Test
    public void successIfDepositBiggerThanMaxAmount() {
        BigDecimal montant = new BigDecimal(1000L);
        long MONTANT_MAXIMAL = 10000L;

        assertDoesNotThrow(
                () -> depositService.checkValidity(montant, MONTANT_MAXIMAL)
            );
    }

    @Test
    public void throwsExceptionIfDepositISBiggerThanMaxAmount() {
        BigDecimal montant = new BigDecimal(10001L);
        long MONTANT_MAXIMAL = 10000L;

        assertThrows(
                TransactionException.class,
                () -> depositService.checkValidity(montant, MONTANT_MAXIMAL)
        );
    }
}
