package ma.octo.assignement.service;

import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class TransferServiceTest {

    @Autowired
    private TransferService transferService;

    @Test
    public void throwsIfEmitterAcountOrBenificiaireAccountIsNull() {
        TransferDto transferDto = new TransferDto();
        Compte compteEmetteur = null;
        Compte compteBenificiaire = null;
        long MONTANT_MAXIMAL = 10000L;
        long MONTANT_MINIMAL = 10L;

        transferDto.setMontant(BigDecimal.valueOf(1000L));

        assertThrows(
                CompteNonExistantException.class,
                () -> transferService.checkValidity(compteEmetteur, compteBenificiaire, transferDto, MONTANT_MAXIMAL, MONTANT_MINIMAL)
        );
    }

    @Test
    public void successIfTransferIsWithinBoundAndAccountsNotNull() {
        TransferDto transferDto = new TransferDto();
        Compte compteEmetteur = new Compte();
        Compte compteBenificiaire = new Compte();
        long MONTANT_MAXIMAL = 10000L;
        long MONTANT_MINIMAL = 10L;

        transferDto.setMontant(BigDecimal.valueOf(1000L));
        compteEmetteur.setSolde(BigDecimal.valueOf(50000L));

        assertDoesNotThrow(
                () -> transferService.checkValidity(compteEmetteur, compteBenificiaire, transferDto, MONTANT_MAXIMAL, MONTANT_MINIMAL)
        );
    }

    @Test
    public void throwsIfSoldeIsNotEnough() {
        TransferDto transferDto = new TransferDto();
        Compte compteEmetteur = new Compte();
        Compte compteBenificiaire = new Compte();
        long MONTANT_MAXIMAL = 10000L;
        long MONTANT_MINIMAL = 10L;

        transferDto.setMontant(BigDecimal.valueOf(1000L));
        compteEmetteur.setSolde(BigDecimal.valueOf(500L));

        assertThrows(
                SoldeDisponibleInsuffisantException.class,
                () -> transferService.checkValidity(compteEmetteur, compteBenificiaire, transferDto, MONTANT_MAXIMAL, MONTANT_MINIMAL)
        );
    }

    @Test
    public void throwsIfTransferAmountIsZero() {
        TransferDto transferDto = new TransferDto();
        Compte compteEmetteur = new Compte();
        Compte compteBenificiaire = new Compte();
        long MONTANT_MAXIMAL = 10000L;
        long MONTANT_MINIMAL = 10L;

        transferDto.setMontant(BigDecimal.valueOf(0L));
        compteEmetteur.setSolde(BigDecimal.valueOf(50000L));

        assertThrows(
                TransactionException.class,
                () -> transferService.checkValidity(compteEmetteur, compteBenificiaire, transferDto, MONTANT_MAXIMAL, MONTANT_MINIMAL)
        );
    }

    @Test
    public void throwsIfTransferAmountIsGreaterThanMaxAmount() {
        TransferDto transferDto = new TransferDto();
        Compte compteEmetteur = new Compte();
        Compte compteBenificiaire = new Compte();
        long MONTANT_MAXIMAL = 10000L;
        long MONTANT_MINIMAL = 10L;

        transferDto.setMontant(BigDecimal.valueOf(10001L));
        compteEmetteur.setSolde(BigDecimal.valueOf(50000L));

        assertThrows(
                TransactionException.class,
                () -> transferService.checkValidity(compteEmetteur, compteBenificiaire, transferDto, MONTANT_MAXIMAL, MONTANT_MINIMAL)
        );
    }

    @Test
    public void throwsIfTransferAmountIsLessThanMinimumAmount() {
        TransferDto transferDto = new TransferDto();
        Compte compteEmetteur = new Compte();
        Compte compteBenificiaire = new Compte();
        long MONTANT_MAXIMAL = 10000L;
        long MONTANT_MINIMAL = 10L;

        transferDto.setMontant(BigDecimal.valueOf(5L));
        compteEmetteur.setSolde(BigDecimal.valueOf(50000L));

        assertThrows(
                TransactionException.class,
                () -> transferService.checkValidity(compteEmetteur, compteBenificiaire, transferDto, MONTANT_MAXIMAL, MONTANT_MINIMAL)
        );
    }
}
