package ma.octo.assignement.repository;

import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.entity.Transfer;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class TransferRepositoryTest {

  @Mock
  private TransferRepository transferRepository;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void findOne() {
    Transfer transfer = new Transfer();
    transfer.setId(999L);

    doReturn(Optional.of(transfer)).when(transferRepository).findById(999L);

    AtomicReference<Transfer> transferResult = new AtomicReference<>();
    transferRepository.findById(999L).ifPresent(transferResult::set);

    assertEquals(999L, transferResult.get().getId());
  }

  @Test
  public void findAll() {
    Transfer transfer = new Transfer();

    doReturn(new ArrayList<>(List.of(transfer))).when(transferRepository).findAll();

    List<Transfer> transferAllResult = transferRepository.findAll();

    assertEquals(1, transferAllResult.size());
  }

  @Test
  public void save() {
    Transfer transfer = new Transfer();
    transfer.setId(999L);

    List<Transfer> listTransfer = new ArrayList<>();

    doAnswer((Answer<Object>) invocation -> {
      listTransfer.add(transfer);
      return null;
    }).when(transferRepository).save(transfer);

    transferRepository.save(transfer);

    assertEquals(1, listTransfer.size());
  }

  @Test
  public void delete() {
    Transfer transfer = new Transfer();
    transfer.setId(999L);

    List<Transfer> listTransfer = new ArrayList<>();
    listTransfer.add(transfer);

    doAnswer((Answer<Object>) invocation -> {
        listTransfer.remove(transfer);
        return null;
      }).when(transferRepository).deleteById(999L);

    transferRepository.deleteById(999L);

    assertEquals(0, listTransfer.size());
  }
}
