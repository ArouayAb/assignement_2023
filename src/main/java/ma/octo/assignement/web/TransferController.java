package ma.octo.assignement.web;

import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.entity.Utilisateur;
import ma.octo.assignement.entity.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.TransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping(path = "/transfers")
class TransferController {

    public static final long MONTANT_MAXIMAL = 10000;
    public static final long MONTANT_MINIMAL = 10;

    Logger LOGGER = LoggerFactory.getLogger(TransferController.class);

    private final CompteRepository accountRepository;
    private final TransferRepository transferRepository;
    private final AuditService auditService;
    private final TransferService transferService;

    TransferController(CompteRepository accountRepository, TransferRepository transferRepository,
                       AuditService auditService, TransferService transferService) {
        this.accountRepository = accountRepository;
        this.transferRepository = transferRepository;
        this.auditService = auditService;
        this.transferService = transferService;
    }

    @GetMapping("listDesTransferts")
    List<Transfer> loadAll() {
        LOGGER.info("List de tous les transfers");
        var all = transferRepository.findAll();
        return CollectionUtils.isEmpty(all) ? null : all;
    }

    @PostMapping("/executerTransfers")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody TransferDto transferDto)
            throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        LOGGER.info("Creation de transaction");
        Compte compteEmetteur = accountRepository.findByNrCompte(transferDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = accountRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());

        transferService.checkValidity(compteEmetteur, compteBeneficiaire, transferDto, MONTANT_MAXIMAL, MONTANT_MINIMAL);
        transferService.makeTransfer(compteEmetteur, compteBeneficiaire, transferDto);
        auditService.auditTransfer(
            "Transfer depuis " +
            transferDto.getNrCompteEmetteur() +
            " vers " + transferDto.getNrCompteBeneficiaire() +
            " d'un montant de " +
            transferDto.getMontant().toString()
        );
    }
}
