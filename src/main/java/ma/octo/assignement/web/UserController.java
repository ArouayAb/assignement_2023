package ma.octo.assignement.web;

import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.entity.Utilisateur;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "users")
public class UserController {

    Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private final CompteRepository accountRepository;
    private final UtilisateurRepository userRepository;

    public UserController(CompteRepository accountRepository, UtilisateurRepository userRepository) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
    }

    @GetMapping("listOfAccounts")
    List<Compte> loadAllCompte() {
        LOGGER.info("List de tous les comptes");
        List<Compte> all = accountRepository.findAll();
        return CollectionUtils.isEmpty(all) ? null : all;
    }

    @GetMapping("lister_utilisateurs")
    List<Utilisateur> loadAllUtilisateur() {
        LOGGER.info("List de tous les utilisateurs");
        List<Utilisateur> all = userRepository.findAll();
        return CollectionUtils.isEmpty(all) ? null : all;
    }

}
