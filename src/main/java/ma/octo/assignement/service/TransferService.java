package ma.octo.assignement.service;

import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.entity.Transfer;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
@Transactional
public class TransferService {
    Logger LOGGER = LoggerFactory.getLogger(TransferService.class);

    private final TransferRepository transferRepository;
    private final CompteRepository accountRepository;

    public TransferService(TransferRepository transferRepository, CompteRepository accountRepository) {
        this.transferRepository = transferRepository;
        this.accountRepository = accountRepository;
    }

    /**
     * Cette methode verifie la validite d'un transfer entre 2 comptes
     * @param compteEmetteur Le compte de l'emetteur
     * @param compteBeneficiaire Le compte du beneficiaire
     * @param transferDto Le dto du transfer contenant les informations necessaire pour effectuer la transfer
     * @param MONTANT_MAXIMAL Le montant maximal a ne pas depasser
     * @throws CompteNonExistantException Exception lever si un des deux comptes est inexistant
     * @throws TransactionException Exception lever si le montant a transferer est egale a 0, inferieur a 10 ou superieur
     * au montant maximal
     * @throws SoldeDisponibleInsuffisantException Exception lever si le solde du compte emetteur est insuffisant pour effectuer le transfer
     */
    public void checkValidity(Compte compteEmetteur, Compte compteBeneficiaire, TransferDto transferDto, long MONTANT_MAXIMAL, long MONTANT_MINIMAL) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        String errMsg;
        if (compteEmetteur == null || compteBeneficiaire == null) {
            errMsg = "Compte Non existant";
            LOGGER.error(errMsg);
            throw new CompteNonExistantException(errMsg);
        } else if (transferDto.getMontant().compareTo(BigDecimal.valueOf(10)) == 0) {
            errMsg = "Montant vide";
            LOGGER.error(errMsg);
            throw new TransactionException(errMsg);
        } else if (transferDto.getMontant().compareTo(BigDecimal.valueOf(MONTANT_MINIMAL)) < 0) {
            errMsg = "Montant minimal de transfer non atteint";
            LOGGER.error(errMsg);
            throw new TransactionException(errMsg);
        } else if (transferDto.getMontant().compareTo(BigDecimal.valueOf(MONTANT_MAXIMAL)) > 0) {
            errMsg = "Montant maximal de transfer dépassé";
            LOGGER.error(errMsg);
            throw new TransactionException(errMsg);
        } else if (compteEmetteur.getSolde().compareTo(transferDto.getMontant()) < 0) {
            errMsg = "Solde insuffisant pour l'utilisateur";
            LOGGER.error(errMsg);
            throw new SoldeDisponibleInsuffisantException(errMsg);
        }
    }

    /**
     * Cette methode effectue le transfer d'un compte emetteur vers un compte benificiaire
     * @param compteEmetteur Le compte de l'emetteur
     * @param compteBeneficiaire Le compte du beneficiaire
     * @param transferDto Le dto du transfer contenant les informations necessaire pour effectuer la transfer
     */
    public void makeTransfer(Compte compteEmetteur, Compte compteBeneficiaire, TransferDto transferDto) {
        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(transferDto.getMontant()));
        accountRepository.save(compteEmetteur);

        compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + transferDto.getMontant().intValue()));
        accountRepository.save(compteBeneficiaire);

        Transfer transfer = new Transfer();
        transfer.setDateExecution(transferDto.getDate());
        transfer.setCompteBeneficiaire(compteBeneficiaire);
        transfer.setCompteEmetteur(compteEmetteur);
        transfer.setMontantTransfer(transferDto.getMontant());

        transferRepository.save(transfer);
    }
}
