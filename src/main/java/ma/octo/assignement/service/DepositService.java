package ma.octo.assignement.service;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.entity.MoneyDeposit;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.MoneyDepositRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;

@Service
@Transactional
public class DepositService {
    Logger LOGGER = LoggerFactory.getLogger(DepositService.class);

    private final CompteRepository accountRepository;
    private final MoneyDepositRepository moneyDepositRepository;

    public DepositService(CompteRepository accountRepository, MoneyDepositRepository moneyDepositRepository) {
        this.accountRepository = accountRepository;
        this.moneyDepositRepository = moneyDepositRepository;
    }

    /**
     * Cette methode verifie la validite du depos
     * @param montant
     * @param MONTANT_MAXIMAL
     * @throws CompteNonExistantException
     * @throws TransactionException
     * @throws SoldeDisponibleInsuffisantException
     */
    public void checkValidity(BigDecimal montant, long MONTANT_MAXIMAL) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        String errMsg;
        if (montant.compareTo(BigDecimal.valueOf(MONTANT_MAXIMAL)) > 0) {
            errMsg = "Montant maximal de depos dépassé";
            LOGGER.error(errMsg);
            throw new TransactionException(errMsg);
        }
    }

    /**
     * Cette methode effetue le deposite d'un montant dans un compte
     * @param account Le compte a accrediter
     * @param depositDto Le dtop contenant les informations necessaire pour le depos
     */
    public void makeDeposit(Compte account, DepositDto depositDto) {
        account.setSolde(account.getSolde().add(depositDto.getMontant()));
        accountRepository.save(account);

        MoneyDeposit moneyDeposit = new MoneyDeposit();
        moneyDeposit.setMontant(depositDto.getMontant());
        moneyDeposit.setMotifDeposit(depositDto.getMotif());
        moneyDeposit.setCompteBeneficiaire(account);
        moneyDeposit.setDateExecution(new Date());
        moneyDeposit.setNom_prenom_emetteur(depositDto.getPrenomEmetteur());

        moneyDepositRepository.save(moneyDeposit);
    }
}
